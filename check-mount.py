#! /usr/bin/python

import sys
import commands

def main():
    mount = sys.argv[1]
    try:
        output = commands.getoutput('df -a '+ mount)
        parts = output.split()
        if (parts[12] == mount):
            print('Mount OK: ' + mount)
            sys.exit(0)

    except Exception:
        print('Mount NG: ' + mount)
        sys.exit(2)

if __name__ == '__main__':
    main()